export class OpenCritic {
    averageScore: number;
    percentRecommended: number;

    static fromMap(openCriticMap: number): OpenCritic {
        const openCritic = new OpenCritic();
        openCritic.averageScore = openCriticMap['average_score'];
        openCritic.percentRecommended = openCriticMap['percent_recommended'];
        return openCritic;
    }
}
export class HLTB {
    mainTime: number;
    mainExtraTime: number;
    completionistTime: number;

    static fromMap(hltbMap: any): HLTB {
        const hltb = new HLTB();
        hltb.mainTime = hltbMap['main_time'];
        hltb.mainExtraTime = hltbMap['main_extra_time'];
        hltb.completionistTime = hltbMap['completionist_time'];
        return hltb;
    }
}
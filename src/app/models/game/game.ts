import { HLTB } from './hltb';
import { OpenCritic } from "./opencritic";

export class Game {
    id: String;
    title: String;
    coverURL: String;
    developer: String;
    publisher: String;
    platforms: String[];
    releaseDate: Date;
    videoID: String;
    hltb: HLTB;
    openCritic: OpenCritic;

    static fromMap(gameMap: any): Game {
        const game = new Game();
        game.id = gameMap['id'];
        game.title = gameMap['title'];
        game.coverURL = gameMap['cover_url'];
        game.developer = gameMap['developer'];
        game.publisher = gameMap['publisher'];
        game.platforms = gameMap['platforms'];
        console.log(gameMap['release_date'].seconds);
        game.releaseDate = new Date(gameMap['release_date'].seconds * 1000);
        game.videoID = gameMap['videoID'];
        game.hltb = HLTB.fromMap(gameMap['hltb_data']);
        game.openCritic = OpenCritic.fromMap(gameMap['opencritic_score'])
        return game;

    }
}
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  displayedColumns: String[] = ['title', 'platforms', 'hltb', 'opencritic'];
  games: Observable<any[]>;

  constructor(private firestore: AngularFirestore) {
    this.games = firestore.collection('games').valueChanges();
  }

  ngOnInit(): void {
  }

}

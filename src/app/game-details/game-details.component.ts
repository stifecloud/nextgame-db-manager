import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Game } from '../models/game/game';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit {

  gameID: String;
  game: any;

  constructor(private route: ActivatedRoute, private firestore: AngularFirestore) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.gameID = params.get('gameID');
      this.firestore.doc(`games/${this.gameID}`).valueChanges().subscribe(gameMap => {
        console.log(gameMap);
        this.game = gameMap;
      })
    });
  }

  saveChanges() {
    //TODO: This still requires to parse elements to correct type
    this.firestore.doc(`games/${this.gameID}`).set(this.game);
  }

}
